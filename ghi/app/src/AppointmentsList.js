import React, { useState, useEffect, Callback, useCallback } from "react";

function AppointmentList() {
    const [appointments, setAppointmentsList] = useState([]);
    const [status, setStatus] = useState('');
    const [filterText, setFilterText] = useState('');


    const fetchData = useCallback(async () => {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);

        if (response.ok){
            const data = await response.json();
            setAppointmentsList(data.appointments);
        }

    }, []);

    useEffect(() => {
        fetchData();
    }, [fetchData]);

    async function handleStatusCancelChange(id, callback) {

        const data = {
            "status": "Canceled",
        };
        const appointmentURL = `http://localhost:8080/api/appointments/${id}/cancel`;

        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(appointmentURL, fetchConfig);

        if (response.ok) {
            setStatus('');
            callback();
        }
    }
    async function handleStatusFinishedChange(id, callback) {

        const data = {
            "status": "Finished",
        };
        const appointmentURL = `http://localhost:8080/api/appointments/${id}/finish`;

        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(appointmentURL, fetchConfig);

        if (response.ok) {
            setStatus('');
            callback();
        }

    }

    function handleFilterChange(e) {
        setFilterText(e.target.value);
    }


    return (
        <div>
            <h1 className="mt-3 mb-3 p-0">Appointments</h1>
            <input
                type="text"
                placeholder="Filter by customer"
                value={filterText}
                onChange={handleFilterChange}
                className="form-control mb-3"
            />
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Reason</th>
                        <th>Vin</th>
                        <th>Technician</th>
                        <th>VIP?</th>
                    </tr>
                </thead>
                <tbody>
                {appointments
                    .filter(appt => 
                        appt.customer.toLowerCase().includes(filterText.toLowerCase()) &&
                        appt.status !== 'canceled' &&
                        appt.status !== 'finished'
                        )
                    .map(appt => {
                        const formattedDate = new Date(appt.date_time).toLocaleDateString();
                        const formattedTime = new Date(appt.date_time).toLocaleTimeString([], { hour: 'numeric', minute: '2-digit', hour12: true });
                        const { vin, sold } = appt.vin;
                        return (
                            <tr key={appt.id}>
                                <td>{appt.customer}</td>                                    
                                <td>{formattedDate}</td>
                                <td>{formattedTime}</td>
                                <td>{appt.reason}</td>
                                <td>{vin}</td>
                                <td>{appt.technician}</td>
                                <td>{sold ? 'VIP' : 'Not VIP'}</td>
                                <td><button type="button" className="btn btn-danger" onClick={() => handleStatusCancelChange(appt.id, fetchData)}>Cancel</button></td>
                                <td><button type="button" className="btn btn-success"onClick={() => handleStatusFinishedChange(appt.id, fetchData)}>Finish</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default AppointmentList;
