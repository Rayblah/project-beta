import React, {useState, useEffect} from "react";

function VehicleModelForm () {
    const [name, setName] = useState('');
    const [picture_url, setPictureURL] = useState('');
    const [manufacturer_id, setManufacturer] = useState('');
    const [manufacturersList, setManufacturersList] = useState([]);

    useEffect(() => { async function fetchManufacturers() {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);

        if (response.ok){
            const data = await response.json();
            setManufacturersList(data.manufacturers);

        }
    }
    fetchManufacturers();
    }, []);

    async function handleSubmit(e) {
        e.preventDefault();
        const data = {
            name,
            picture_url, 
            manufacturer_id,
        }
        const vehicleModelURL = "http://localhost:8100/api/models/";


        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };


        const response = await fetch(vehicleModelURL, fetchConfig);

        if (response.ok) {
            setName('');
            setPictureURL('');
            setManufacturer('');
        }
    }

    function handleNameChange(e){
        const value = e.target.value;
        setName(value);
    }
    function handlePictureURLChange(e){
        const value = e.target.value;
        setPictureURL(value);
    }
    function handleManufacturerChange(e){
        const value = e.target.value;
        setManufacturer(value);
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a Model</h1>
                <form onSubmit={handleSubmit} id="create-manufacturer-form">
                    <div className="form-floating mb-3">
                        <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="text" id="name" className="form-control" />
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={picture_url} onChange={handlePictureURLChange} placeholder="Picture URL" required type="text" name="text" id="picture_url" className="form-control" />
                        <label htmlFor="picture_url">Picture URL</label>
                    </div>
                    <div className="mb-3">
                        <select value={manufacturer_id} onChange={handleManufacturerChange} required name="manufacturer" id="manufacturer" className="form-select">
                            <option value="manufacturersList">Choose a Manufacturer</option>
                            {manufacturersList.map(manu => {
                                return(
                                    <option key={manu.id} value={manu.id}>
                                        {manu.name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
   );
}
export default VehicleModelForm;
