import React, { useEffect, useState } from "react";

function AutomobileList() {
    const [autos, setAutos] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch(url);

        if(response.ok){
            const data = await response.json();
            setAutos(data.autos);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    return (
        <div>
        <h1 className="mt-3 mb-3 p-0">Automobile List</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <td>VIN</td>
                    <td>Color</td>
                    <td>Year</td>
                    <td>Model</td>
                    <td>Manufacturer</td>
                    <td>Sold</td>
                </tr>
            </thead>
            <tbody>
                {autos.map(auto => {
                    const {
                        vin,
                        color,
                        year,
                        model: {
                        name: modelName,
                        manufacturer: { name: manufacturerName },
                        },
                        sold,
                    } = auto;
                    return (
                        <tr key={vin}>
                            <td> {vin} </td>
                            <td> {color} </td>
                            <td> {year} </td>
                            <td> {modelName} </td>
                            <td> {manufacturerName} </td>
                            <td> {sold ? 'Yes': 'No'} </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    </div>
    )
}
export default AutomobileList;
