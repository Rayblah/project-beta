import React, { useState } from 'react';
function CustomerForm() {
    const[name, setName] = useState('')
    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }
    const[address, setAddress] = useState('')
    const handleAddressChange = (event) => {
        const value = event.target.value
        setAddress(value)
    }
    const[phoneNumber, setPhoneNumber] = useState('');
    const handlePhoneNumberChange = (event) => {
        const value = event.target.value
        setPhoneNumber(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const customer = {
            name: name,
            address: address,
            phone_number: phoneNumber,
        }

        const customerUrl = 'http://localhost:8090/api/customers/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(customer),
            headers: {
                "Content-Type": "application/json",
            }
        }

        const response = await fetch(customerUrl, fetchConfig)
        if(response.ok) {
            const newCustomer = await response.json();
            console.log(newCustomer)

            setName('')
            setAddress('')
            setPhoneNumber('')
            window.location.replace('/customers')
        } else {
            console.log('error')
        }
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Customer</h1>
                    <form onSubmit={handleSubmit} id="create-salesperson-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/> 
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleAddressChange} placeholder="Address" required type="text" name="address" id="address" className="form-control"/> 
                            <label htmlFor="address">Address</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePhoneNumberChange} placeholder="Phone Number" required type="text" name="phone_number" id="phone_number" className="form-control"/> 
                            <label htmlFor="phone_number">Phone Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default CustomerForm