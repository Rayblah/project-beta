import React, { useState, useEffect } from 'react';

function SalesHistory() {
    const[salespersons, setSalespersons] = useState([])
    const[salesperson, setSalesperson] = useState('')
    const handleSalespersonChange = (event) => {
        const value = parseInt(event.target.value);
        setSalesperson(value)
    }
    const[sales, setSales] = useState([])

    const fetchSalesData = async () => {
        const salesUrl = 'http://localhost:8090/api/sales/'
        const response = await fetch(salesUrl)
        console.log(response)

        if(response.ok) {
            const data = await response.json()
            console.log(data)
            setSales(data.sales)
        }
    }

    const fetchSalespersonsData = async () => {
        const salespersonsUrl = 'http://localhost:8090/api/salespeople/'
        const response = await fetch(salespersonsUrl)

        if(response.ok) {
            const data = await response.json()
            setSalespersons(data.salespersons)
        }
    }


    useEffect(() => {
        fetchSalespersonsData()
    }, [])

    useEffect(() => {
        fetchSalesData()
    }, [])

    // Filter by sales
    const salesFiltered = () => {
        const filtered = sales.filter(sale => sale.salesperson.id === salesperson);
        return filtered;
    }

    return (
        <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Salesperson History</h1>
                    <form id="select-salesperson-form">
                        <div className="mb-3">
                        <select value={salesperson} onChange={handleSalespersonChange} required id="salesperson" name="salesperson" className="form-select">
                            <option>Choose a Salesperson</option>
                            {salespersons.map(salesperson => {
                                return (
                                    <option key={salesperson.id} value={salesperson.id}>
                                        {salesperson.name}
                                    </option>
                                )
                            })}
                            </select>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Salesperson</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {salesFiltered().map(sale => {
                    return (
                        <tr key={sale.id}>
                            <td>{sale.salesperson.name}</td>
                            <td>{sale.customer.name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.price}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    </>
    )    
}
export default SalesHistory;