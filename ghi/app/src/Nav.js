import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-link">
              <NavLink className="nav-link" to="/manufacturers">Manufacturers</NavLink>
              <NavLink className="nav-link" to="/manufacturers/new">Create a Manufacturer</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" to="/models">Vehicle Models</NavLink>
              <NavLink className="nav-link" to="/models/new">Create a Model</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/salespeople">Salespeople</NavLink>
              <NavLink className="nav-link active" to="/salespeople/new">Add a Salesperson</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" to="/customers">Customers</NavLink>
              <NavLink className="nav-link" to="/customers/new">Add a Customer</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" to="/sales">Sales</NavLink>
              <NavLink className="nav-link" to="/sales/new">Add a Sale</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" to="/technicians">Technicians</NavLink>
              <NavLink className="nav-link" to="/technicians/new">Add a Technician</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" to="/appointments">Service Appointments</NavLink>
              <NavLink className="nav-link" to="/appointments/new">Create a Service Appointment</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" to="/appointments/history">Service History</NavLink>
              <NavLink className="nav-link" to="/sales/history">Sales History</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" to="/automobiles">Automobiles List</NavLink>
              <NavLink className="nav-link" to="/automobiles/add">Add an Automobiles</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
