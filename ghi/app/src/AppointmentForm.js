import React, { useEffect, useState } from "react";

function AppointmentForm() {
    const [date, setDate] = useState('');
    const [status, setStatus] = useState('Created');
    const [time, setTime] = useState('');
    const [reason, setReason] = useState('');
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [technician, setTechnician] = useState();
    const [techs, setTechs] = useState([]);


    useEffect(() => { async function fetchTechs() {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTechs(data.technicians);
        }
    }

    fetchTechs();
    }, []);

    async function handleSubmit(e) {
        e.preventDefault();
        const date_time = new Date(`${date}T${time}`);
        const data = {
            date_time,
            reason,
            status,
            vin,
            customer,
            technician,
        };
        const appointmentURL = 'http://localhost:8080/api/appointments/';

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(appointmentURL, fetchConfig);

        if (response.ok) {

            setDate('');
            setTime('');
            setReason('');
            setVin('');
            setCustomer('');
            setTechnician('');
        }
 
    }

    function handleDateChange(e) {
        const value = e.target.value;
        setDate(value);
    }

    function handleTimeChange(e) {
        const value = e.target.value;
        setTime(value);
    }
    
    function handleReasonChange(e){
        const value = e.target.value;
        setReason(value);
    }
    function handleVinChange(e){
        const value = e.target.value;
        setVin(value);
    }
    function handleCustomerChange(e){
        const value = e.target.value;
        setCustomer(value);
    }
    function handleTechChange(e){
        const value = e.target.value;
        setTechnician(value);
    }
    
    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create an Appointment</h1>
                <form onSubmit={handleSubmit} id="create-appointment-form">
                    <div className="form-floating mb-3">
                        <input value={date} onChange={handleDateChange} placeholder="Date" required type="date" name="date" id="date" className="form-control" />
                        <label htmlFor="date">Date</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={time} onChange={handleTimeChange} placeholder="Time" required type="time" name="time" id="time" className="form-control" />
                        <label htmlFor="time">Time</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={reason} onChange={handleReasonChange} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                        <label htmlFor="reason">Reason</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={vin} onChange={handleVinChange} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                        <label htmlFor="vin">Vin</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={customer} onChange={handleCustomerChange} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
                        <label htmlFor="customer">Customer</label>
                    </div>
                    <div className="mb-3">
                        <select value={technician} onChange={handleTechChange} required name="technician" id="technician" className="form-select">
                            <option value="technician">Choose a technician</option>
                            {techs.map(tech => {
                                const techName = tech.first_name + ' ' + tech.last_name;
                                return(
                                    <option key={tech.id} value={techName}>
                                        {techName}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
    )

}

export default AppointmentForm;
