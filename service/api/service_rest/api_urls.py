from django.urls import path
from service_rest.api_views import api_technician_list, api_appointment_list, api_appointment_finish, api_appointment_cancel, api_technician_detail, api_appointment_detail

urlpatterns = [
    path("technicians/", api_technician_list, name="api_technician_list"),
    path("technicians/<int:id>/", api_technician_detail, name="api_technician_detail"),
    path("appointments/", api_appointment_list, name="api_appointment_list"),
    path("appointments/<int:id>/", api_appointment_detail, name="api_appointment_detail"),
    path("appointments/<int:id>/cancel", api_appointment_cancel, name="api_appointment_cancel"),
    path("appointments/<int:id>/finish", api_appointment_finish, name="api_appointment_finish")
]
