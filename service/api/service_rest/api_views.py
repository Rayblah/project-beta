from common.json import ModelEncoder
import json
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .models import Technician, Appointment, AutomobileVO

class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]

class AutomobileVOListEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]

class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "status",
        "customer",
        "technician",
        "reason",
        "id",
        "vin",
    ]
    encoders = {
        "vin": AutomobileVOListEncoder(),
        "sold": AutomobileVOListEncoder(),
    }

    def get_extra_data(self, o):
        return {"technician": f'{o.technician.first_name} {o.technician.last_name}'}


@require_http_methods(["GET", "POST"])
def api_technician_list (request):
    if request.method == "GET":
        content = Technician.objects.all()
        tech_list = []
        for tech in content:
            d = {
                "first_name": tech.first_name,
                "last_name": tech.last_name,
                "employee_id": tech.employee_id,
                "id":tech.id
            }
            tech_list.append(d)
        return JsonResponse(
            {"technicians":tech_list}
        )
    else: 

        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"Message": "Incorrect Technician info"},
                status = 400,
            )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_technician_detail(request, id):
    try:
        technician = Technician.objects.get(id=id)
    except Technician.DoesNotExist:
        return JsonResponse({"Message": "Technician not found"}, status=404)

    if request.method == "GET":
        technician = Technician.objects.get(id=id)
        return JsonResponse(
            technician,
            encoder=TechnicianDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count>0})
    else:
        content = json.loads(request.body)
        Technician.objects.filter(id=id).update(**content)
        tech = Technician.objects.get(id=id)
        return JsonResponse(
            tech,
            encoder=TechnicianDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_appointment_list (request):
    if request.method == "GET":
        content = Appointment.objects.all()
        appt_list = []
        for appt in content:
            d = {
                "Customer": appt.customer,
                "reason": appt.reason,
                "vin": appt.vin,
                "status": appt.status,
                "date_time": appt.date_time,
                "technician": appt.technician,
                "id":appt.id
            }
            appt_list.append(d)
        return JsonResponse(
            {"appointments": content},
            encoder=AppointmentDetailEncoder,
        )
    else:
        content = json.loads(request.body)

        technician_name = content.get("technician")
        vin = content.get("vin")

        try: #checking tech in Tech obj. 
            if technician_name:
                first_name, last_name = technician_name.split(" ")
                technician = Technician.objects.get(first_name=first_name, last_name=last_name)
                content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"Message":"Invalid Technician Name"},
                status=400,
            )
        if vin:
            try: #checking vin number in Auto Obj, if none, createing. 
                automobile= AutomobileVO.objects.get(vin=vin)
            except AutomobileVO.DoesNotExist:
                automobile_data = {
                    "vin":vin,
                    "sold": False,
                }
                automobile= AutomobileVO.objects.create(**automobile_data)
            content["vin"] = automobile
        
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def api_appointment_detail(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
    except Appointment.DoesNotExist:
        return JsonResponse({"Message": "Appointment not found"}, status=404)

    if request.method == "GET":
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count>0})
    

    

@require_http_methods(["PUT"])
def api_appointment_cancel(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.status = "canceled"
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        return JsonResponse(
            {"Message":"invalid appointment id"},
            status=400,
        )

@require_http_methods(["PUT"])
def api_appointment_finish(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.status = "finished"
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        return JsonResponse(
            {"Message":"invalid appointment id"},
            status=400,
        )
    
