# CarCar

Team:

* Raymond Quach - Automobile Sales
* David Plaskett - Automobile Service

[TOC]

## Design


## Service microservice

I have created models for the "Appointment", "AutomobileVO", and "Technician" classes. The "vin" model is a foreign key to the AutomobileVO and the "technician" model is a foreign key to Technician.

Created views for each request type. Set up the urls for each view and registered the url to the service_project. Tested each view in Insomnia and found working. 

Updated the polloer.py file to pull for "vins" from the Inventory app. 



## Instructions
TO RUN THE PROJECT:
- Fork from the repository at https://gitlab.com/Rayblah/project-beta/
- Open the terminal
- Clone the repository to your computer
- Change the directory to the cloned repository and run the following commands:
1. ```docker volume create beta-data```
2. ```docker-compose build```
3. ```docker-compose up```
- When the docker containers are fully up and running, navigate to the webpage 'http://localhost:3000' to view the running application. 

## Inventory microservice
Three models were created within inventory microservice:
1. Manufacturer - Represents name of the manufacturer
2. Vehicle Model - Represents name of the model, picture url of the vehicle, and manufacturer
3. Automobiles - Represents the color, year, VIN, and model of the vehicle

**Inventory API Port: 8090**

***Manufacturer***

Request method: GET, List manufacturer
http://localhost:8090/api/manufacturers/

Request method: POST, Create a customer
http://localhost:8090/api/manufacturers/

***Vehicle Models***

Request method: GET, List models
http://localhost:8090/api/models/

Request method: POST, Create a model
http://localhost:8090/api/models/

***Automobile***

Request method: GET, List automobiles
http://localhost:8090/api/automobiles/

Request method: POST, Create a automobile
http://localhost:8090/api/automobiles/

## Service microservice

I have created models for the "Appointment", "AutomobileVO", and "Technician" classes. The "vin" model is a foreign key to the AutomobileVO and the "technician" model is a foreign key to Technician.

Created views for each request type. Set up the urls for each view and registered the url to the service_project. Tested each view in Insomnia and found working. 

Updated the polloer.py file to pull for "vins" from the Inventory app. 

Three models were created for the service microservice:
1. Technician - Represents the employees name and ID
2. Appointment - Represents the start date, appointment time, VIN, customer's name, and reason for visit
3. AutomobileVO - Represents the VIN number of the vehicle. 

**Inventory API Port: 8090**

***Technician***

Request method: GET, List technicians
http://localhost:8080/api/technicians/

Request method: POST, Create a technician
http://localhost:8080/api/technicians/

Request method: DELETE, Delete a technician
http://localhost:8080/api/technicians/id/


***Service Appointment***

Request method: GET, List appointments
http://localhost:8080/api/appointments/

Request method: POST, Create a appointments
http://localhost:8080/api/appointments/

Request method: DELETE, Delete a appointments
http://localhost:8080/api/appointments/id/

Request method: PUT, Set appointment status to canceled
http://localhost:8080/api/appointments/:id/cancel

Request method: PUT, Set appointment status to finished
http://localhost:8080/api/appointments/:id/finish

## Sales microservice
Four models were created within the sales microservice:
1. AutomobileVO - Represents VIN, color, and year of the automobile. 
2. Customer - Represents customers name, address, and phone number
3. Salesperson - Represents salesperson name, and employee ID
4. Salesrecord - Represents salesperson employee ID, name, customer name, automobile VIN, and price. 

**Sales API Port: 8090**

***Customer***

Request method: GET, List customers
http://localhost:8090/api/customers/

Request method: POST, Create a customer
http://localhost:8090/api/customers/

Request method: DELETE, Delete a customer
http://localhost:8090/api/customers/id/


***Salesperson***

Request method: GET, List salespeople
http://localhost:8090/api/salespeople/

Request method: POST, Create a salesperson
http://localhost:8090/api/salespeople/

Request method: DELETE, Delete a salesperson
http://localhost:8090/api/salespeople/id/

***Salesrecord***

Request method: GET, List sales
http://localhost:8090/api/sales/

Request method: POST, Create a sales
http://localhost:8090/api/sales/

Request method: GET, Show a sale
http://localhost:8090/api/sales/id/

Request method: DELETE, Delete a sale
http://localhost:8090/api/sales/id/


## Project Diagram
![Alt text](https://gitlab.com/Rayblah/project-beta/-/raw/main/Images%20for%20README/Project_Diagram.png?inline=false "Project Diagram")

# CRUD Route Documentation

## Manufacturers

### List manufacturers

- **GET**: `/api/manufacturers/`

### Create a manufacturer

- **POST**: `/api/manufacturers/`
  - Headers: `Content-Type: application/json`
  - Body: JSON object (manufacturer data)

### Get a specific manufacturer

- **GET**: `/api/manufacturers/:id/`
  - Path Parameters: `id` (manufacturer ID)

### Update a specific manufacturer

- **PUT**: `/api/manufacturers/:id/`
  - Path Parameters: `id` (manufacturer ID)
  - Headers: `Content-Type: application/json`
  - Body: JSON object (updated manufacturer data)

### Delete a specific manufacturer

- **DELETE**: `/api/manufacturers/:id/`
  - Path Parameters: `id` (manufacturer ID)

## Vehicle Models

### List vehicle models

- **GET**: `/api/models/`

### Create a vehicle model

- **POST**: `/api/models/`
  - Headers: `Content-Type: application/json`
  - Body: JSON object (vehicle model data)

### Get a specific vehicle model

- **GET**: `/api/models/:id/`
  - Path Parameters: `id` (vehicle model ID)

### Update a specific vehicle model

- **PUT**: `/api/models/:id/`
  - Path Parameters: `id` (vehicle model ID)
  - Headers: `Content-Type: application/json`
  - Body: JSON object (updated vehicle model data)

### Delete a specific vehicle model

- **DELETE**: `/api/models/:id/`
  - Path Parameters: `id` (vehicle model ID)

## Automobile Information

### List automobiles

- **GET**: `/api/automobiles/`

### Create an automobile

- **POST**: `/api/automobiles/`
  - Headers: `Content-Type: application/json`
  - Body: JSON object (automobile data)

### Get a specific automobile

- **GET**: `/api/automobiles/:vin/`
  - Path Parameters: `vin` (vehicle identification number)

### Update a specific automobile

- **PUT**: `/api/automobiles/:vin/`
  - Path Parameters: `vin` (vehicle identification number)
  - Headers: `Content-Type: application/json`
  - Body: JSON object (updated automobile data)

### Delete a specific automobile

- **DELETE**: `/api/automobiles/:vin/`
  - Path Parameters: `vin` (vehicle identification number)
  
## Technicians

### List technicians

- **GET**: `/api/technicians/`

### Create a technician

- **POST**: `/api/technicians/`
  - Headers: `Content-Type: application/json`
  - Body: JSON object (technician data)

### Delete a specific technician

- **DELETE**: `/api/technicians/:id/`
  - Path Parameters: `id` (technician ID)

## Appointments

### List appointments

- **GET**: `/api/appointments/`

### Create an appointment

- **POST**: `/api/appointments/`
  - Headers: `Content-Type: application/json`
  - Body: JSON object (appointment data)

### Delete an appointment

- **DELETE**: `/api/appointments/:id/`
  - Path Parameters: `id` (appointment ID)

### Set appointment status to canceled

- **PUT**: `/api/appointments/:id/cancel`
  - Path Parameters: `id` (appointment ID)

### Set appointment status to finished

- **PUT**: `/api/appointments/:id/finish`
  - Path Parameters: `id` (appointment ID)

## Salespeople

### List salespeople

- **GET**: `/api/salespeople/`

### Create a salesperson

- **POST**: `/api/salespeople/`
  - Headers: `Content-Type: application/json`
  - Body: JSON object (salesperson data)

### Delete a specific salesperson

- **DELETE**: `/api/salespeople/:id/`
  - Path Parameters: `id` (salesperson ID)

## Customers

### List customers

- **GET**: `/api/customers/`

### Create a customer

- **POST**: `/api/customers/`
  - Headers: `Content-Type: application/json`
  - Body: JSON object (customer data)

### Delete a specific customer

- **DELETE**: `/api/customers/:id/`
  - Path Parameters: `id` (customer ID)

## Sales

### List sales

- **GET**: `/api/sales/`

### Create a sale

- **POST**: `/api/sales/`
  - Headers: `Content-Type: application/json`
  - Body: JSON object (sale data)

### Delete a sale

- **DELETE**: `/api/sales/:id/`
  - Path Parameters: `id` (sale ID)

## Value Objects
- Manufacturers
- Automobiles
